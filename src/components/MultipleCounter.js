import React from "react";
import { CounterGroup } from "./CounterGroup";
import { CounterSizeGenerator } from "./CounterSizeGenerator";
import {CounterSum } from "./CounterSum";
import { useSelector } from "react-redux";

export function MultipleCounter() {
  const countList = useSelector(state => state.counter.countList)
  const sum = countList.reduce((prev, curr) => prev+curr, 0)

  return <>
    <CounterSizeGenerator size={countList.length}/>
    <CounterSum sum={sum}/>
    <CounterGroup countList={countList}/>
  </>
}

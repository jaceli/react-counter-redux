import { useDispatch } from "react-redux";
import { updateSize } from "./counterSlice";

export function CounterSizeGenerator({size}) {

    const dispatch = useDispatch();

    function handleSizeChange(e) {
      const number = parseInt(e.target.value);
      const size = isNaN(number) ? 0 : number;
      dispatch(updateSize(size));
    }
  
    return <div>
      <span>Size:</span>
      <input value={size || ''} onChange={handleSizeChange}/>
    </div>;
  }
  
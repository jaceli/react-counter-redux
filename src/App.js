import './App.css';
import {MultipleCounter} from './components/MultipleCounter';

function App() {
  return (
    <div className='container'>
      <MultipleCounter />
    </div>
  );
}

export default App;

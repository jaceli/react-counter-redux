import { render, screen } from "@testing-library/react"
import Counter from "./Counter"

describe('Counter component test', () => {
  test('should render Counter component with count props', () => {
    const count = 10
    render(<Counter count={count} />)
    const countNode = screen.getByTestId("count")
    expect(countNode).toBeInTheDocument()
    expect(countNode.textContent).toEqual(`${count}`)
  })
})
import Counter from "./Counter";
import { useDispatch } from "react-redux";
import {increment, decrement} from './counterSlice'

export function CounterGroup({countList}) {
  const dispatch = useDispatch();

  return <>
    <p>Here are {countList.length} counters</p>
    {countList.map((count, index) => (
      <Counter key={index} count={count} increase={() => dispatch(increment(index))} decrease={() => dispatch(decrement(index))}  />
    ))}
  </>;
}


import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    countList: [0],
  },
  reducers: {
    updateSize: (state, action) => {
      const size = action.payload
      state.countList = new Array(size).fill(0)
    },

    increment: (state, action) => {
      const index = action.payload
      state.countList[index] += 1
    },

    decrement: (state, action) => {
      const index = action.payload
      state.countList[index] -= 1
    },
  }
})

// Action creators are generated for each case reducer function
export const { updateSize, increment,decrement} = counterSlice.actions

// console.log(counterSlice.actions);

// console.log(counterSlice.actions.updateSize(333))

// console.log(counterSlice.actions.increment([1,2]))

export default counterSlice.reducer
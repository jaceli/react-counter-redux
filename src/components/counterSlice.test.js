import counterSlice, {updateSize, increment, decrement} from "./counterSlice"


describe('counter reducer test', () => {
  it('should generate update size action when call updateSize', () => {
    const updateSizeAction = updateSize(3)
    expect(
      updateSizeAction
    ).toEqual({"payload": 3, "type": "counter/updateSize"})
  })

  it('should get init state', () => {
    const initState = counterSlice(undefined, {})
    expect(initState).toEqual({
      countList: [0]
    })
  })

    it('should change counter list with updateSize reducer', () => {
    const state = counterSlice(undefined, updateSize(3))
    expect(state).toEqual({
      countList: [0,0,0]
    })
    })
  
    it('should increase 1', () => {
    const state = counterSlice({countList: [0, 1]}, increment(1))
    expect(state).toEqual({
      countList: [0,2]
    })
    })
  
      test('should decrease 1', () => {
    const state = counterSlice({countList: [0, 1]}, decrement(1))
    expect(state).toEqual({
      countList: [0,0]
    })
  })
})
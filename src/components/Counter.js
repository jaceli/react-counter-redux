import './Counter.css'

const Counter = (props) => {
    return (
        <div className="counter">
            <button className='button' onClick={props.decrease}>-</button>
            <span id='count'>{props.count}</span>
            <button className='button' onClick={props.increase}>+</button>
        </div>
    )
}

export default Counter